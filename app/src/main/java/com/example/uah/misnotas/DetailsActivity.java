package com.example.uah.misnotas;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.uah.misnotas.helpers.SubjectSQLiteHelper;
import com.example.uah.misnotas.models.Subject;

import java.text.DecimalFormat;

/**
 * Actividad para mostrar los detalles de una asignatura
 */
public class DetailsActivity extends AppCompatActivity {
    //Etiqueta para clasificar los eventos de esta actividad al llamar a Log.d
    private static final String LOG_TAG = DetailsActivity.class.getSimpleName();

    private SubjectSQLiteHelper ssdbh;
    private SQLiteDatabase db;

    private Subject mSubject;

    private TextView mName;
    private TextView mGrade;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        // Conexión al Helper de SQLite para futuras consultas
        ssdbh = new SubjectSQLiteHelper(this);

        // Definición de la Action Bar personalizada
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);

        // Inicialización de los textos del layout
        mName = findViewById(R.id.label_details_name);
        mGrade = findViewById(R.id.label_details_grade);

        // Carga del id de la asignatura enviado mediante Intent
        Bundle bundle = this.getIntent().getExtras();
        int subject_id = bundle.getInt(MainActivity.EXTRA_SUBJECT_ID);
        loadSubjectData(subject_id);
    }

    // Carga los datos de la asignatura a visualizar
    private void loadSubjectData(int id) {
        // Establece una conexión con la BBDD en modo lectura
        db = ssdbh.getReadableDatabase();

        Cursor c = ssdbh.getSubjectById(db, id);
        if (c.moveToFirst()) {
            do {
                String name = c.getString(0);
                float grade = c.getFloat(1);
                Log.d(LOG_TAG, id + "-" + name + "-" + grade);
                mSubject = new Subject(id, name, grade);
            } while (c.moveToNext());
        }
        db.close();

        // Se actualizan los campos del layout con la información de la asignatura
        mName.setText(mSubject.getName());
        mGrade.setText(new DecimalFormat(".0").format(Float.valueOf(mSubject.getGrade())));
    }

    @Override
    // Creación del menú personalizado situado en el Action Bar
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    // Manejador de eventos de las acciones del Action Bar
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete:
                showConfirmActionDialog();
                break;
            case R.id.action_edit:
                openFormActivity();
                break;
            case android.R.id.home:
                finish();
        }
        return false;
    }

    // Muestra un cuadro de diálogo para que el usuario confirme si desea continuar con la operación
    // de borrado
    protected void showConfirmActionDialog() {
        new AlertDialog.Builder(this)
                .setMessage("La asignatura se eliminará de la aplicación ¿Desea continuar?")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteSubject();
                    }
                })
                .setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .show();
    }

    // Inicia la actividad FormActivity mediante un intent
    private void openFormActivity() {
        // Se crea el intent explícito, con destinatario FormActivity
        Intent intent = new Intent(DetailsActivity.this, FormActivity.class);

        // Se crea el objeto bundle con el id de la asignatura a editar y el identificador
        // de la operación
        Bundle bundle = new Bundle();
        bundle.putInt(MainActivity.EXTRA_SUBJECT_ID, mSubject.getId());
        bundle.putInt(MainActivity.EXTRA_OPERATION, MainActivity.SUBJECT_EDIT);
        intent.putExtras(bundle);

        // Inicio de la actividad, con cierre de la actual
        startActivity(intent);
        finish();
    }

    // Comunica a la actividad el resultado del borrado de una asignatura mediante un Intent
    private void sendDeleteSubjectIntent() {
        // Se crea el intent explícito, con destinatario MainActivity
        Intent intent = new Intent(DetailsActivity.this, MainActivity.class);

        // Se crea el objeto bundle con el nombre de la asignatura eliminada y el identificador
        // de la operación
        Bundle bundle = new Bundle();
        bundle.putString(MainActivity.EXTRA_SUBJECT_NAME, mSubject.getName());
        bundle.putInt(MainActivity.EXTRA_OPERATION, MainActivity.SUBJECT_DELETE);
        intent.putExtras(bundle);

        // Inicio de la actividad, con cierre de la actual
        startActivityIfNeeded(intent, MainActivity.SUBJECT_DELETE);
        finish();
    }

    // Elimina una asignatura mediante una llamada al helper de SQLite
    private void deleteSubject() {
        // Establece una conexión con la BBDD en modo escritura
        db = ssdbh.getWritableDatabase();
        if (db != null) {
            int id = mSubject.getId();
            ssdbh.deleteSubject(db, id);
            sendDeleteSubjectIntent();
        }
    }
}