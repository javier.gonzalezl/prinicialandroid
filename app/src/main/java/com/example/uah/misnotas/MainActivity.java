package com.example.uah.misnotas;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.uah.misnotas.adapters.SubjectListAdapter;
import com.example.uah.misnotas.helpers.SubjectSQLiteHelper;
import com.example.uah.misnotas.models.Subject;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

/**
 * Actividad principal de la aplicación
 */
public class MainActivity extends AppCompatActivity {

    //Etiqueta para clasificar los eventos de esta actividad al llamar a Log.d
    private static final String LOG_TAG = MainActivity.class.getSimpleName();

    // Listado de asignaturas presentes en la aplicación
    private ArrayList<Subject> mSubjectList = new ArrayList();

    // Códigos de operación
    public static final int SUBJECT_ADD = 1;
    public static final int SUBJECT_EDIT = 2;
    public static final int SUBJECT_DELETE = 3;

    public static final String EXTRA_OPERATION = "com.example.uah.misnotas.extra.OPERATION";
    public static final String EXTRA_SUBJECT_ID = "com.example.uah.misnotas.extra.SUBJECT_ID";
    public static final String EXTRA_SUBJECT_NAME = "com.example.uah.misnotas.extra.SUBJECT_NAME";


    private RecyclerView mRecyclerView;
    private SubjectListAdapter mAdapter;

    private SubjectSQLiteHelper ssdbh;
    SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Botón flotante y listener de su evento click
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent subjectForm = new Intent(MainActivity.this, FormActivity.class);
                Bundle b = new Bundle();
                b.putInt(EXTRA_OPERATION, SUBJECT_ADD);
                subjectForm.putExtras(b);
                startActivity(subjectForm);
            }
        });

        // Comprueba si hay un intent y actúa en consecuencia si lo hay
        Intent intent = this.getIntent();
        if (intent != null) handleIncomingIntent(intent);

        // Conexión al Helper de SQLite para futuras consultas
        ssdbh = new SubjectSQLiteHelper(this);
        loadSubjects();

        mRecyclerView = findViewById(R.id.reyclerview);
        // Creamos el adaptador y le pasamos como argumentos los datos para generar cada elemento de la lista
        mAdapter = new SubjectListAdapter(this, mSubjectList);
        // Conectamos el adaptador con el RecyclerView
        mRecyclerView.setAdapter(mAdapter);
        // Definimos el tipo de layout manager que tendrá el RecyclerView por defecto
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    // Función auxiliar para mostrar un toast por pantalla informando del resultado de la última
    // operación realizada
    public void displayToast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    // Carga el listado de asignaturas con la información que haya en base de datos
    private void loadSubjects() {
        // Establece una conexión con la BBDD en modo lectura
        db = ssdbh.getReadableDatabase();

        Cursor c = ssdbh.getAllSubjects(db);
        if (c.moveToFirst()) {
            do {
                int id = c.getInt(0);
                String name = c.getString(1);
                float grade = c.getFloat(2);
                Log.d(LOG_TAG, id + "-" + name + "-" + grade);
                mSubjectList.add(new Subject(id, name, grade));
            } while (c.moveToNext());
        }
        db.close();
    }

    // Analiza el contenido de un intent y muestra el resultado de la la operación recibida dentro
    // de él
    private void handleIncomingIntent(Intent intent) {
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            int operation = bundle.getInt(EXTRA_OPERATION);
            String subject_name = bundle.getString(EXTRA_SUBJECT_NAME);
            switch (operation) {
                case SUBJECT_ADD:
                    displayToast("Se ha creado la asignatura " + subject_name);
                    break;
                case SUBJECT_DELETE:
                    displayToast("Se ha eliminado la asignatura " + subject_name);
                    break;
                case SUBJECT_EDIT:
                    displayToast("Se ha modificado la asignatura " + subject_name);
                    break;
            }
        }
    }
}