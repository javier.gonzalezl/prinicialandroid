package com.example.uah.misnotas.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.uah.misnotas.models.Subject;

public class SubjectSQLiteHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "Subjects.db";
    public static final int DATABASE_VERSION = 1;

    public SubjectSQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    String createSQL = "CREATE TABLE subject (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, grade REAL)";
    String dropSQL = "DROP TABLE IF EXISTS subject";

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(createSQL);
        insertDefaultData(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(dropSQL);
        onCreate(db);
    }

    // Carga la BBDD con datos iniciales en su creación
    public void insertDefaultData(SQLiteDatabase db) {
        db.execSQL("INSERT INTO subject (name, grade) VALUES ('Cloud Computing y contenedores', 9.1)");
        db.execSQL("INSERT INTO subject (name, grade) VALUES ('UX', 9.1)");
        db.execSQL("INSERT INTO subject (name, grade) VALUES ('Frameworks Frontend', 9.1)");
        db.execSQL("INSERT INTO subject (name, grade) VALUES ('Frameworks Backend', 9.1)");
    }

    // Método auxiliar para insertar elementos en la BBDD
    public void insertSubject(SQLiteDatabase db, String name, float grade) {
        ContentValues newRecord = new ContentValues();
        newRecord.put("name", name);
        newRecord.put("grade", grade);
        db.insert("subject", null, newRecord);
    }

    // Método auxiliar para eliminar elementos de la BBDD
    public void deleteSubject(SQLiteDatabase db, int id) {
        String[] args = new String[] {Integer.toString(id)};
        db.delete("subject", "id=?", args);
    }

    // Método auxiliar para actualizar elementos en la BBDD
    public void updateSubject(SQLiteDatabase db, Subject subject) {
        ContentValues values = new ContentValues();
        values.put("name", subject.getName());
        values.put("grade", subject.getGrade());
        String[] args = new String[] {Integer.toString(subject.getId())};
        db.update("subject", values, "id=?", args);
    }

    // Método auxiliar para obtener un elemento de la BBDD por su id
    public Cursor getSubjectById(SQLiteDatabase db, int id) {
        String[] fields = new String[] {"name", "grade"};
        String[] args = new String[] {Integer.toString(id)};
        Cursor c = db.query("subject", fields, "id=?", args, null, null, null);
        return c;
    }

    // Método auxiliar para obtener todos los elementos de la BBDD
    public Cursor getAllSubjects(SQLiteDatabase db) {
        String[] fields = new String[] {"id", "name", "grade"};
        Cursor c = db.query("subject", fields, null, null, null, null, null);
        return c;
    }
}
