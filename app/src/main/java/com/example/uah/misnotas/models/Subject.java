package com.example.uah.misnotas.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Subject implements Parcelable {


    public static final Creator CREATOR = new Creator() {
        public Subject createFromParcel(Parcel in) {
            return new Subject(in);
        }

        public Subject[] newArray(int size) {
            return new Subject[size];
        }
    };

    private int id;
    private String name; // nombre de la asignatura
    private float grade; // calificación de la asignatura

    public Subject(int id, String name, float grade) {
        this.id = id;
        this.name = name;
        this.grade = grade;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getGrade() {
        return grade;
    }

    public void setGrade(float grade) {
        this.grade = grade;
    }

    // Constructor para recuperar el objeto después de haberse enviado como Parceable en un Intent
    private Subject(Parcel in){
        this.id = in.readInt();
        this.name = in.readString();
        this.grade = in.readFloat();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeFloat(this.grade);
    }

    @Override
    public String toString() {
        return "Subject{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", grade=" + grade +
                '}';
    }
}
