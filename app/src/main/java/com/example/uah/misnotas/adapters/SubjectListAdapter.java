package com.example.uah.misnotas.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.uah.misnotas.DetailsActivity;
import com.example.uah.misnotas.MainActivity;
import com.example.uah.misnotas.R;
import com.example.uah.misnotas.models.Subject;

import java.util.ArrayList;

/**
 * Adaptador que permite definir cada elemento del RecyclerView como un objeto de tipo Subject y
 * gestionar toda la lógica relacionada con el layout de estos elementos
 *
 * Fuente: https://google-developer-training.github.io/android-developer-fundamentals-course-concepts-v2/unit-2-user-experience/lesson-4-user-interaction/4-5-c-recyclerview/4-5-c-recyclerview.html
 */
public class SubjectListAdapter extends RecyclerView.Adapter<SubjectListAdapter.SubjectViewHolder> {

    private final ArrayList<Subject> mSubjectList;
    private LayoutInflater mInflater;

    class SubjectViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public final TextView subjectItemView;
        final SubjectListAdapter mAdapter;

        public SubjectViewHolder(View itemView, SubjectListAdapter adapter) {
            super(itemView);
            subjectItemView = itemView.findViewById(R.id.subject);
            this.mAdapter = adapter;
            itemView.setOnClickListener(this);
        }

        @Override
        // Al hacer click en un elemento del RecyclerView se va a querer iniciar la actividad
        // de detalles de una asignatura.
        public void onClick(View v) {
            // Se coge la posición del elemento pulsado para, a continuación, acceder al objeto vinculado
            int mPosition = getLayoutPosition();
            Subject element = mSubjectList.get(mPosition);

            // Se crea el intent para iniciar la acitividad Details
            Intent subjectDetails = new Intent(v.getContext(), DetailsActivity.class);
            Bundle b = new Bundle();
            b.putInt(MainActivity.EXTRA_SUBJECT_ID, element.getId()); //se envía el id para poder recuperar luego el objeto
            subjectDetails.putExtras(b);
            v.getContext().startActivity(subjectDetails);
        }
    }

    public SubjectListAdapter(Context context, ArrayList<Subject> subjectList) {
        mInflater = LayoutInflater.from(context);
        this.mSubjectList = subjectList;
    }

    @Override
    public SubjectViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mItemView = mInflater.inflate(R.layout.subjectlist_item, parent, false);
        return new SubjectViewHolder(mItemView, this);
    }

    @Override
    public void onBindViewHolder(SubjectViewHolder holder, int position) {
        Subject mCurrent = mSubjectList.get(position);
        holder.subjectItemView.setText(mCurrent.getName());
    }

    @Override
    public int getItemCount() {
        return mSubjectList.size();
    }
}
