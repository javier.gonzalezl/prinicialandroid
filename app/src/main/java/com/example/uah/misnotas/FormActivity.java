package com.example.uah.misnotas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.uah.misnotas.helpers.SubjectSQLiteHelper;
import com.example.uah.misnotas.models.Subject;

import java.text.DecimalFormat;


/**
 * Actividad para gestionar el formulario usado para la creación y edición de una asignatura
 */
public class FormActivity extends AppCompatActivity {
    //Etiqueta para clasificar los eventos de esta actividad al llamar a Log.d
    private static final String LOG_TAG = FormActivity.class.getSimpleName();

    private SubjectSQLiteHelper ssdbh;
    private SQLiteDatabase db;

    private Subject mSubject;
    private int mOperation;
    private EditText mName;
    private EditText mGrade;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);

        // Conexión al Helper de SQLite para futuras consultas
        ssdbh = new SubjectSQLiteHelper(this);

        // Se oculta el título de la actividad en la Action Bar
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        // Inicialización de los campos de texto del layout
        mName = findViewById(R.id.editText_name);
        mGrade = findViewById(R.id.editText_grade);

        // Carga de la operación a realizar, enviada mediante Intent
        Bundle bundle = this.getIntent().getExtras();
        mOperation = bundle.getInt(MainActivity.EXTRA_OPERATION);

        // En caso de que la operación a realizar en el formulario sea la edición y no la creación,
        // hay que inicializar sus campos con la información de la asignatura recibida a través de un
        // Intent
        if (mOperation == MainActivity.SUBJECT_EDIT) {
            int subject_id = bundle.getInt(MainActivity.EXTRA_SUBJECT_ID);
            initializeFormFields(subject_id);
        }
    }

    private void initializeFormFields(int id) {
        // Establece una conexión con la BBDD en modo lectura
        db = ssdbh.getReadableDatabase();

        Cursor c = ssdbh.getSubjectById(db, id);
        if (c.moveToFirst()) {
            do {
                String name = c.getString(0);
                float grade = c.getFloat(1);
                Log.d(LOG_TAG, id + "-" + name + "-" + grade);
                mSubject = new Subject(id, name, grade);
            } while (c.moveToNext());
        }
        db.close();

        // Se actualizan los campos del layout con la información de la asignatura
        mName.setText(mSubject.getName());
        mGrade.setText(new DecimalFormat(".0").format(Float.valueOf(mSubject.getGrade())));
    }

    // Listener para el botón del formulario.
    public void sendForm(View v) {
        if (isGradeValid()) {
            switch (mOperation) {
                case MainActivity.SUBJECT_ADD:
                    createSubject();
                    break;
                case MainActivity.SUBJECT_EDIT:
                    updateSubject();
                    break;
            }
        } else {
            displayToast("La calificación debe ser un número entre 0 y 10");
            mGrade.setText("");
        }
    }

    // Actualiza una asignatura mediante una llamada al helper de SQLite
    private void updateSubject() {
        // Establece una conexión con la BBDD en modo escritura
        db = ssdbh.getWritableDatabase();
        if (db != null) {
            String name = mName.getText().toString();
            mSubject.setName(name);
            mSubject.setGrade(Float.parseFloat(mGrade.getText().toString()));
            ssdbh.updateSubject(db, mSubject);
            sendResponseIntent(MainActivity.SUBJECT_EDIT, name);
        }
    }

    // Crea una asignatura mediante una llamada al helper de SQLite
    private void createSubject() {
        // Establece una conexión con la BBDD en modo escritura
        db = ssdbh.getWritableDatabase();
        if (db != null) {
            String name = mName.getText().toString();
            float grade = Float.parseFloat(mGrade.getText().toString());
            ssdbh.insertSubject(db, name, grade);
            sendResponseIntent(MainActivity.SUBJECT_ADD, name);
        }
    }

    // Comunica a la actividad el resultado de la acción realizado con el envío del formulario mediante un Intent
    private void sendResponseIntent(int operation, String name) {
        // Se crea el intent explícito, con destinatario MainActivity
        Intent intent = new Intent(FormActivity.this, MainActivity.class);

        // Se crea el objeto bundle con el nombre de la asignatura eliminada y el identificador
        // de la operación
        Bundle bundle = new Bundle();
        bundle.putString(MainActivity.EXTRA_SUBJECT_NAME, name);
        bundle.putInt(MainActivity.EXTRA_OPERATION, operation);
        intent.putExtras(bundle);

        // Inicio de la actividad, con cierre de la actual
        startActivityIfNeeded(intent, operation);
        finish();
    }

    // Validador del campo de texto de la calificación. Dicho campo debe estar relleno y contener
    // un número comprendido entre el 0 y el 10
    private boolean isGradeValid() {
        String textValue = mGrade.getText().toString();
        if (textValue.matches("")) {
            return false;
        }
        final float grade = Float.parseFloat(textValue);
        return grade >= 0 && grade <= 10;
    }

    // Función auxiliar para mostrar un toast por pantalla si hay un error en la validación del
    // formulario
    public void displayToast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }
}